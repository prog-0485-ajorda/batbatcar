package es.jorda.BatBatCar.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Scanner;

public class GestorIO {

    private static Scanner scanner;

    private static final int MIN = 0;

    static {
        scanner = new Scanner(System.in);
    }

    public static int getInt(String mensaje) {
        do {
            System.out.print(mensaje + " : ");
            if (!scanner.hasNextInt()) {
                System.out.print("Debe introducir un entero");
                scanner.next();
            } else {
                int leido =  scanner.nextInt();
                limpiarBuffer();
                return leido;
            }
        } while (true);
    }

    public static int getInt(String mensaje, int maximoOpciones) {
        do {
            System.out.print(mensaje + " : ");
            if (!scanner.hasNextInt()) {
                System.out.print("Debe introducir un entero");
                scanner.next();
            } else {
                int leido =  scanner.nextInt();
                if (leido <= maximoOpciones && leido > MIN){
                    limpiarBuffer();
                    return leido;
                }
                System.out.printf("Debes introducir un numero entre [%d-%d]", MIN, maximoOpciones);
            }
        } while (true);
    }

    public static String getString(String mensaje) {
        System.out.print(mensaje + " : ");
        return scanner.nextLine();
    }

    public static float getFloat(String mensaje) {
        do {
            System.out.print(mensaje + " : ");
            if (!scanner.hasNextInt()) {
                System.out.print("Debe introducir un entero");
                scanner.next();
            } else {
                int leido =  scanner.nextInt();
                limpiarBuffer();
                return leido;
            }
        } while (true);
    }

    public LocalDateTime getTime(String mensaje){
        /**do{
            String localDateTime = getString(mensaje);
            try{
                return LocalDateTime.parse(localDateTime, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
            } catch (DateTimeParseException e){

            }

        }while ()
        */
        return null;
    }

    public static boolean getBool(String mensaje){
        do {
            System.out.print(mensaje + " : ");
            String respuesta = scanner.next();
            if (respuesta.equals("s")) {
                return true;
            }else if(respuesta.equals("n")) {
                return false;
            }else {
                System.out.println("Debes introducir solo 's' o 'n'");
            }
        } while (true);
    }

    private static void limpiarBuffer() {
        scanner.nextLine();
    }
}
