package es.jorda.BatBatCar.views;

import de.vandermeer.asciitable.AT_Row;
import de.vandermeer.asciitable.AsciiTable;
import de.vandermeer.skb.interfaces.transformers.textformat.TextAlignment;
import es.jorda.BatBatCar.modelo.entidades.Reserva;
import es.jorda.BatBatCar.modelo.entidades.types.Viatje;

import java.util.ArrayList;
import java.util.Set;

public class ViewListaReserva {

    private ArrayList<Reserva> listaReservas;

    private static final int ANCHO_TABLA = 100;

    public ViewListaReserva(ArrayList<Reserva> listaReservas) {
        this.listaReservas = listaReservas;
    }

    private AsciiTable buildASCIITable() {
        AsciiTable view = new AsciiTable();
        view.addRule();
        AT_Row fila1 = view.addRow("*", "*", "*","*", "*");
        fila1.setTextAlignment(TextAlignment.CENTER);
        view.addRule();
        fila1 = view.addRow(null, null, null, null,"Reservas Realizadas");
        fila1.setTextAlignment(TextAlignment.CENTER);
        view.addRule();
        fila1 = view.addRow("Cod. Reserva", "Usuario", "Fecha Realizada",null, "Plazas reservadas");
        fila1.setTextAlignment(TextAlignment.CENTER);
        view.addRule();
        for (Reserva reserva : listaReservas) {
            fila1 = view.addRow((reserva.getReservCode()), reserva.getUser(), reserva.getData(), null, reserva.getRequestedPlaces());
            fila1.setTextAlignment(TextAlignment.CENTER);
            view.addRule();
        }
        return view;
    }

    @Override
    public String toString() {
        return buildASCIITable().render(ANCHO_TABLA);
    }
}
