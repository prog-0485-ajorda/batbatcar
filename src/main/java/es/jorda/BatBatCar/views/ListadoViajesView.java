package es.jorda.BatBatCar.views;

import de.vandermeer.asciitable.AT_Row;
import de.vandermeer.skb.interfaces.transformers.textformat.TextAlignment;
import es.jorda.BatBatCar.modelo.entidades.types.Viatje;

import java.util.Set;

import de.vandermeer.asciitable.AsciiTable;

public class ListadoViajesView {

    private Set<Viatje> viajes;

    private static final int ANCHO_TABLA = 100;

    public ListadoViajesView(Set<Viatje> viajes) {
        this.viajes = viajes;
    }

    private AsciiTable buildASCIITable() {

        AsciiTable view = new AsciiTable();
        view.addRule();
        AT_Row fila1 = view.addRow("*", "*", "*","*", "*","*", "*");
        fila1.setTextAlignment(TextAlignment.CENTER);
        view.addRule();
        fila1 = view.addRow(null, null, null, null, null, null,"Listado Viajes");
        fila1.setTextAlignment(TextAlignment.CENTER);
        view.addRule();
        fila1 = view.addRow("Codigo", "Ruta", "Precio(€)",null, "Fecha Salida","Tipo Viaje", "Plazas Disponibles");
        fila1.setTextAlignment(TextAlignment.CENTER);
        view.addRule();
        for (Viatje viatje : viajes) {
            fila1 = view.addRow((viatje.getCodViaje()), viatje.getRuta(), viatje.getPrecio(),null, viatje.getFechaSalida(), viatje.getTipo(), viatje.getPlazasDisponibles());
            fila1.setTextAlignment(TextAlignment.CENTER);
            view.addRule();
        }
        return view;
    }

    @Override
    public String toString() {
        return buildASCIITable().render(ANCHO_TABLA);
    }
}
