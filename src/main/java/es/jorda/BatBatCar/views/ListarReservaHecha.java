package es.jorda.BatBatCar.views;

import de.vandermeer.asciitable.AT_Row;
import de.vandermeer.asciitable.AsciiTable;
import de.vandermeer.skb.interfaces.transformers.textformat.TextAlignment;
import es.jorda.BatBatCar.modelo.entidades.Reserva;

public class ListarReservaHecha {

    private Reserva reserva;

    private static final int ANCHO_TABLA = 65;

    public ListarReservaHecha(Reserva reserva){
        this.reserva = reserva;
    }

    public AsciiTable ListarReserva(){
        AsciiTable view = new AsciiTable();
        view.addRule();
        AT_Row fila1 = view.addRow("*", "*");
        fila1.setTextAlignment(TextAlignment.CENTER);
        view.addRule();
        fila1 = view.addRow( null,"Reserva con condigo " + reserva.getReservCode());
        fila1.setTextAlignment(TextAlignment.CENTER);
        view.addRule();
        fila1 = view.addRow("Codigo: ",reserva.getUser());
        fila1.setTextAlignment(TextAlignment.CENTER);
        view.addRule();
        fila1 = view.addRow("Plazas: ",reserva.getRequestedPlaces());
        fila1.setTextAlignment(TextAlignment.CENTER);
        view.addRule();
        fila1 = view.addRow("Fecha realizacion: ",reserva.getData());
        fila1.setTextAlignment(TextAlignment.CENTER);
        view.addRule();
        return view;
    }

    @Override
    public String toString() {
        return ListarReserva().render();
    }
}
