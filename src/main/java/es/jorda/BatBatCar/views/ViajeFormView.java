package es.jorda.BatBatCar.views;

import es.jorda.BatBatCar.modelo.entidades.types.Viatje;
import es.jorda.BatBatCar.modelo.entidades.types.ViatjeCancelable;
import es.jorda.BatBatCar.modelo.entidades.types.ViatjeExclusivo;
import es.jorda.BatBatCar.modelo.entidades.types.ViatjeFlexible;
import es.jorda.BatBatCar.utils.GestorIO;

import java.time.LocalDateTime;

public class ViajeFormView {

    public static final int VIAJE_DEFAULT = 1;

    public static final int VIAJE_CANCELABLE = 2;

    public static final int VIAJE_EXCLUSIVO = 3;

    public static final int VIAJE_FLEXIBLE = 4;

    public int getTipoViaje(){
        return GestorIO.getInt("Selecciona el tipo de viaje \n " +
                "1.Viaje por defecto\n" +
                "2.Viaje Cancelable\n" +
                "3.Viaje Flexible\n" +
                "4.Viaje Exclusivo", 4);
    }

    public Viatje getNewViaje(){
        String ruta = GestorIO.getString("Introduce la ruta: ");
        LocalDateTime fechaSalida = LocalDateTime.of(GestorIO.getInt("Introduce el año"),
                GestorIO.getInt("Introduce el mes: "),
                GestorIO.getInt("Introduce el dia: "), GestorIO.getInt("Introduce la hora: "),
                GestorIO.getInt("Introducir los minutos: "));
        long duracion = GestorIO.getInt("Introduce la duracion estimada");
        float precio = GestorIO.getFloat("Introducir el precio: ");
        int plazasDisponibles = GestorIO.getInt("Introducir las plazas disponibles");
        String duenyo = GestorIO.getString("Introduce el nombre del dueño del viaje: ");

        switch (getTipoViaje()){
            case VIAJE_CANCELABLE:
                return new ViatjeCancelable(ruta, fechaSalida, duracion, precio, plazasDisponibles, duenyo);
            case VIAJE_EXCLUSIVO:
                return new ViatjeExclusivo(ruta, fechaSalida, duracion, precio, duenyo);
            case VIAJE_FLEXIBLE:
                return new ViatjeFlexible(ruta, fechaSalida, duracion, precio, plazasDisponibles, duenyo);
            default:
                return new Viatje(ruta, fechaSalida, duracion, precio, plazasDisponibles, duenyo);
        }

    }
}
