package es.jorda.BatBatCar.menu.opciones;

public class OpcionSalir extends Opcion {

    public OpcionSalir() {
        super("Salir");
    }

    @Override
    public void ejecutar() {
        System.out.println("Adiós");
        setFinalizar(true);
    }

}
