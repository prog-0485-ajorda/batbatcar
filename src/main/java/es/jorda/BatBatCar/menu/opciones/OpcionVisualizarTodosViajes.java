package es.jorda.BatBatCar.menu.opciones;

import es.jorda.BatBatCar.controller.AgenteViajesController;

public class OpcionVisualizarTodosViajes extends Opcion {

    private AgenteViajesController agenteViajesController;

    public OpcionVisualizarTodosViajes(AgenteViajesController agenteViajesController) {
        super("Listar todos los viajes");
        this.agenteViajesController = agenteViajesController;
    }

    @Override
    public void ejecutar() {
        agenteViajesController.listarViajes();
    }

}
