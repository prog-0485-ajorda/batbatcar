package es.jorda.BatBatCar.menu.opciones;

import es.jorda.BatBatCar.controller.AgenteViajesController;

public class OpcionCancelarViatje extends Opcion{

    private final AgenteViajesController agenteViajesController;

    public OpcionCancelarViatje(AgenteViajesController agenteViajesController) {
        super("Cancelar Viaje");
        this.agenteViajesController = agenteViajesController;
    }

    @Override
    public void ejecutar() {
        agenteViajesController.cancelarViaje();
    }
}
