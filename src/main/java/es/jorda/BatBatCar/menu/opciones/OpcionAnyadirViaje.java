package es.jorda.BatBatCar.menu.opciones;

import es.jorda.BatBatCar.controller.AgenteViajesController;

public class OpcionAnyadirViaje extends Opcion{

    private final AgenteViajesController agenteViajesController;

    public OpcionAnyadirViaje(AgenteViajesController agenteViajesController) {
        super("Añadir Viaje");
        this.agenteViajesController = agenteViajesController;
    }

    @Override
    public void ejecutar() {
        agenteViajesController.añadirViaje();

    }
}
