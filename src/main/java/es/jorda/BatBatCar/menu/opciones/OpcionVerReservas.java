package es.jorda.BatBatCar.menu.opciones;

import es.jorda.BatBatCar.controller.AgenteViajesController;

public class OpcionVerReservas extends Opcion {
    private AgenteViajesController agenteViajesController;

    public OpcionVerReservas(AgenteViajesController agenteViajesController) {
        super("Ver reservas de un viaje");
        this.agenteViajesController = agenteViajesController;
    }

    @Override
    public void ejecutar() {
        agenteViajesController.verReservas();
    }
}
