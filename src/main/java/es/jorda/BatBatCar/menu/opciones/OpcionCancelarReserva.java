package es.jorda.BatBatCar.menu.opciones;

import es.jorda.BatBatCar.controller.AgenteViajesController;

public class OpcionCancelarReserva extends Opcion{
    private final AgenteViajesController agenteViajesController;

    public OpcionCancelarReserva(AgenteViajesController agenteViajesController) {
        super("Cancelar reserva");
        this.agenteViajesController = agenteViajesController;
    }

    @Override
    public void ejecutar() {
        agenteViajesController.cancelarReserva();
    }
}
