package es.jorda.BatBatCar.menu.opciones;

import es.jorda.BatBatCar.controller.AgenteViajesController;

public class OpcionBuscarViajeYReserva extends Opcion{
    private final AgenteViajesController agenteViajesController;

    public OpcionBuscarViajeYReserva(AgenteViajesController agenteViajesController) {
        super("Buscar Viaje");
        this.agenteViajesController = agenteViajesController;
    }

    @Override
    public void ejecutar() {
        agenteViajesController.buscarViaje();
    }
}
