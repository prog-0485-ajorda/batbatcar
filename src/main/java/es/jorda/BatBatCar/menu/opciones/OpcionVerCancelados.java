package es.jorda.BatBatCar.menu.opciones;

import es.jorda.BatBatCar.controller.AgenteViajesController;

public class OpcionVerCancelados extends Opcion{

    private final AgenteViajesController agenteViajesController;

    public OpcionVerCancelados(AgenteViajesController agenteViajesController) {
        super("Ver viajes Cancelados");
        this.agenteViajesController = agenteViajesController;
    }

    @Override
    public void ejecutar() {
        agenteViajesController.listarViajesCancelados();
    }
}
