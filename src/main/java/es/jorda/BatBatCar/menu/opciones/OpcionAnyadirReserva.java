package es.jorda.BatBatCar.menu.opciones;

import es.jorda.BatBatCar.controller.AgenteViajesController;

public class OpcionAnyadirReserva extends Opcion{

    private final AgenteViajesController agenteViajesController;

    public OpcionAnyadirReserva(AgenteViajesController agenteViajesController) {
        super("Añadir Reserva a un viaje");
        this.agenteViajesController = agenteViajesController;
    }

    @Override
    public void ejecutar() {
        agenteViajesController.anyadirReserva();
    }
}
