package es.jorda.BatBatCar;

import es.jorda.BatBatCar.controller.AgenteViajesController;
import es.jorda.BatBatCar.menu.Menu;
import es.jorda.BatBatCar.menu.opciones.*;

public class BatBatCar {
    public static void main(String[] args) {
        AgenteViajesController agenteViajesController = new AgenteViajesController();
        Menu menu = new Menu("BatBatCar");
        menu.anyadir(new OpcionVisualizarTodosViajes(agenteViajesController));
        menu.anyadir(new OpcionAnyadirViaje(agenteViajesController));
        menu.anyadir(new OpcionCancelarViatje(agenteViajesController));
        menu.anyadir(new OpcionAnyadirReserva(agenteViajesController));
        menu.anyadir(new OpcionVerCancelados(agenteViajesController));
        menu.anyadir(new OpcionVerReservas(agenteViajesController));
        menu.anyadir(new OpcionCancelarReserva(agenteViajesController));
        menu.anyadir(new OpcionBuscarViajeYReserva(agenteViajesController));
        menu.anyadir(new OpcionSalir());
        menu.ejecutar();
    }
}
