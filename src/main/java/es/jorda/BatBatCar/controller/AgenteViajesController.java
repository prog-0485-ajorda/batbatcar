package es.jorda.BatBatCar.controller;

import es.jorda.BatBatCar.exceptions.CodigoViajeNoValidoException;
import es.jorda.BatBatCar.exceptions.ReservaNoEnonctradaExcepcion;
import es.jorda.BatBatCar.exceptions.ReservaNoValidaException;
import es.jorda.BatBatCar.exceptions.ReservasLlenasException;
import es.jorda.BatBatCar.modelo.entidades.Reserva;
import es.jorda.BatBatCar.modelo.entidades.types.Viatje;
import es.jorda.BatBatCar.modelo.repositorio.ViajesRepository;
import es.jorda.BatBatCar.utils.GestorIO;
import es.jorda.BatBatCar.views.*;


import java.util.ArrayList;
import java.util.Set;

public class AgenteViajesController {
    private final ViajesRepository viajesRepository;

    public AgenteViajesController() {
        this.viajesRepository = new ViajesRepository();
    }

    public void listarViajes() {
        Set<Viatje> viajes = viajesRepository.findAll();
        listarViajes(viajes);
    }

    public void añadirViaje(){
        ViajeFormView viajeFormView = new ViajeFormView();
        Viatje viaje = viajeFormView.getNewViaje();
        viajesRepository.anyadir(viaje);
    }

    public void cancelarViaje(){
        Set<Viatje> viajes = viajesRepository.findAllViajesCancelable();
        listarViajes(viajes);
        CancelarViajeForm cancelarViajeForm = new CancelarViajeForm();
        viajesRepository.canecelarViaje(cancelarViajeForm.codigoACancelar());
    }

    public void anyadirReserva(){
        verViajesDisponibles();
        Reserva reserva;
        do {
            try{
                Viatje viatje = viajesRepository.getViaje(GestorIO.getInt("Seleccione el viaje para la reserva mediante su codigo "));
                reserva = viatje.reservar(GestorIO.getString("Introduce tu nombre: "),
                        GestorIO.getInt("Introduce numero de plazas que quieres reservar: "));
                break;
            }catch (CodigoViajeNoValidoException |ReservaNoValidaException| ReservasLlenasException e){
                System.out.println(e.getMessage());
            }
        }while (true);
        ListarReservaHecha listarReservaHecha = new ListarReservaHecha(reserva);
        System.out.println(listarReservaHecha);
    }

    public void listarViajesCancelados(){
        Set<Viatje> viajes = viajesRepository.findAllCancelados();
       listarViajes(viajes);
    }

    public void verReservas(){
        verViajesDisponibles();
        Viatje viatje;
        do {
            try {
                viatje = viajesRepository.getViaje(GestorIO.getInt("Selecciona el viaje a consultar reservas"));
                break;
            }catch (CodigoViajeNoValidoException e){
                System.out.println(e.getMessage());
            }
        }while (true);
        ArrayList<Reserva> lisraReserva = viatje.getListaReservas();
        ViewListaReserva listadoViajesView = new ViewListaReserva(lisraReserva);
        System.out.println(listadoViajesView);
    }

    private void verViajesDisponibles(){
        Set<Viatje> viajes = viajesRepository.findAll(true);
        listarViajes(viajes);
    }

    public void cancelarReserva(){
        verReservas();
        int codReservaEliminar =  GestorIO.getInt("Introduce el codigo de la reserva a cancelar");
        Viatje viatje;
        do {
            try {
                viatje = viajesRepository.getViajeConReserva(codReservaEliminar);
                break;
            }catch (ReservaNoEnonctradaExcepcion e){
                e.getMessage();
            }
        }while (true);
        viatje.encontrarReserva(codReservaEliminar);
    }

    public void buscarViaje(){
        String ciudadABuscar = GestorIO.getString("Escribe la ciudad a buscar");
        Set<Viatje> viajes = new ViajesRepository().buscarCiudad(ciudadABuscar);
        if (viajes.size() == 0){
            System.out.println("La ciudad no aparece en ningun viaje");
            return;
        }else {
            ListadoViajesView listadoViajesView = new ListadoViajesView(viajes);
            System.out.println(listadoViajesView);
        }
        if (GestorIO.getBool("Quieres reservar un viaje?[s(si)-n(no)")){
            anyadirReserva(viajes);
        }
    }

    private void anyadirReserva(Set viajes){
        Reserva reserva = null;
        try{
            int cod = GestorIO.getInt("Seleccione el viaje para la reserva mediante su codigo ");
            for (Object viaje: viajes ) {
                Viatje viaje1 = (Viatje) viaje;
                if (viaje1.getCodViaje() == cod){
                    reserva = viaje1.reservar(GestorIO.getString("Introduce tu nombre: "),
                            GestorIO.getInt("Introduce numero de plazas que quieres reservar: "));
                    break;
                }
            }
        }catch (ReservaNoValidaException| ReservasLlenasException e){
            System.out.println(e.getMessage());
        }
        if (reserva != null){
           listarReserva(reserva);
        }
    }

    public void listarReserva(Reserva reserva) {
        ListarReservaHecha listarReservaHecha = new ListarReservaHecha(reserva);
        System.out.println(listarReservaHecha);
    }

    public void listarViajes(Set viajes) {
        ListadoViajesView listadoViajesView = new ListadoViajesView(viajes);
        System.out.println(listadoViajesView);
    }
}
