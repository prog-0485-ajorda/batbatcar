package es.jorda.BatBatCar.modelo.repositorio;


import es.jorda.BatBatCar.exceptions.CodigoViajeNoValidoException;
import es.jorda.BatBatCar.exceptions.ReservaNoEnonctradaExcepcion;
import es.jorda.BatBatCar.exceptions.ReservaNoValidaException;
import es.jorda.BatBatCar.exceptions.ReservasLlenasException;
import es.jorda.BatBatCar.modelo.entidades.Reserva;
import es.jorda.BatBatCar.modelo.entidades.types.Viatje;
import es.jorda.BatBatCar.modelo.entidades.types.ViatjeCancelable;
import es.jorda.BatBatCar.modelo.entidades.types.ViatjeExclusivo;
import es.jorda.BatBatCar.modelo.entidades.types.ViatjeFlexible;

import java.time.LocalDateTime;
import java.util.*;

public class ViajesRepository {


    private HashSet<Viatje> viajes;

    private HashSet<Viatje> viajescancelados;

    public ViajesRepository() {
        init();
    }


    @Override
    public int hashCode() {
        return Objects.hash(viajes);
    }

    /**
     * Permite cancelar un viaje  dado un codigo
     *
     * @param codigoViaje int
     */
        public void canecelarViaje(int codigoViaje){
            for (Viatje viaje: viajes) {
                if ((codigoViaje) == viaje.getCodViaje()){
                    viajes.remove(viaje);
                    viajescancelados.add(viaje);
                    System.out.printf("Viaje con codigo %d se ha cancelado", codigoViaje);
                    return;
                }
            }
            System.out.println("Viaje no encontrado");
    }

    /**
     * Añade un nuevo viaje al repositorio
     *
     * @param viaje
     */
    public void anyadir(Viatje viaje) {
        viajes.add(viaje);
    }

    /**
     * Obtiene el número de viajes actualmente registrados
     *
     * @return
     */
    public int getNumViajes() {
        return viajes.size();
    }

    /**
     * Obtiene un viaje a partir de su código
     *
     * @param codViaje
     * @return El viaje obtenido
     * El método debe lanzar una excepción en caso de no encontrar el viaje
     */
    public Viatje getViaje(int codViaje) throws CodigoViajeNoValidoException {
        for (Viatje viatje: findAll(true)) {
            if (viatje.getCodViaje() == codViaje){
                return viatje;
            }
        }
        throw new CodigoViajeNoValidoException();
    }

    /**
     * Obtiene el viaje asociado a una reserva dada
     *
     * @param codReserva int
     * @return El viaje relacionado
     * El método debe lanzar una excepción en caso de no encontrar una reserva con ese código
     */

    public Viatje getViajeConReserva(int codReserva) throws ReservaNoEnonctradaExcepcion {
        for (Viatje viaje:viajes ) {
            for (Reserva reserva: viaje.getListaReservas()) {
                if (reserva.getReservCode() == codReserva){
                    return viaje;
                }
            }
        }
        throw new ReservaNoEnonctradaExcepcion();
    }

    public Set<Viatje> buscarCiudad( String ciudad){
       Set<Viatje> viajesConLaCiudad = new HashSet<>();
       Set<Viatje> viajesDisponibles = findAllViajesCancelable();
       for (Viatje viaje: viajesDisponibles) {
            StringTokenizer tokens = new StringTokenizer(viaje.getRuta(),"-");
            while (tokens.hasMoreTokens()){
                if (tokens.nextToken().equals(ciudad)){
                    viajesConLaCiudad.add(viaje);
                }
            }
        }
        return viajesConLaCiudad;
    }

    /**
     * Obtiene un conjunto de viajes de la modalidad proporcionada
     *
     * @param viajeClass la clase de los viajes a buscar
     * @return La lista de los viajes de la clase proporcionada
     */
    public Set<Viatje> findAll(Class<? extends Viatje> viajeClass) {
        Set<Viatje> viajesMismoTipo = new HashSet<>();
        for (Viatje viaje: viajes) {
            if (viaje.getClass().equals(viajeClass)){
                viajesMismoTipo.add(viaje);
            }
        }
        return viajesMismoTipo;
    }

    public Set<Viatje> findAllCancelados() {
        return viajescancelados;
    }


    public Set<Viatje> findAllViajesCancelable() {
        Set<Viatje> viajesCancelables = findAll(true);
        Set<Viatje> viajesNoCancelables = findAll(ViatjeExclusivo.class);
        viajesCancelables.removeIf(viajesNoCancelables::contains);
        return viajesCancelables;
    }

        /**
         * Obtiene un conjunto de viajes en función de su disponibilidad
         *
         * @param disponible true --> obtendrá sólo los disponibles
         * @return La lista de los viajes que cumplan con el criterio
         */
        public Set<Viatje> findAll(boolean disponible) {
            Set<Viatje> viajesDisponibles = new HashSet<>();
            for (Viatje viaje: viajes){
                if (!viaje.isCerrado() == disponible){
                    viajesDisponibles.add(viaje);
                }
            }
            return viajesDisponibles;
        }

        /**
         * Obtiene todos los viajes
         *
         * @return HastSet de todos los viajes
         */
        public Set<Viatje> findAll() {
            return viajes;
        }

        private void init() {
            try {
                viajes = new HashSet<>();
                viajescancelados = new HashSet<>();
                Viatje viaje1 = new Viatje("Madrid-Murcia-Alicante",
                        LocalDateTime.of(2022, 5, 21, 12, 0, 0), 4, 5f, 4, "Pepe");
                viaje1.reservar("Alex", 2);
                viaje1.reservar("Roberto", 1);
                viajes.add(viaje1);
                Viatje viaje2 = new ViatjeExclusivo( "Alicante-Valencia",
                        LocalDateTime.of(2022, 4, 15, 12, 0,0), 10, 5f, "Pepe");
                viajes.add(viaje2);

                Viatje viaje3 = new ViatjeCancelable( "Madrid-Barcelona",
                        LocalDateTime.of(2022, 5, 12, 12, 0, 0), (3*60), 5f, 2, "Pepe");
                viajes.add(viaje3);

                Viatje viaje4 = new ViatjeFlexible( "Alcoy-Cocentaina", LocalDateTime.of(2022, 8, 21, 12, 0, 0), 10, 5f, 4, "Pepe");
                viaje4.reservar("Maria Patiño", 2);
                viajes.add(viaje4);

                Viatje viaje5Ida = new ViatjeExclusivo("Alcoy-Alicante",
                        LocalDateTime.of(2022, 9, 9, 12, 0, 0), 45, 4f, "Pepe");
                viajes.add(viaje5Ida);
                Viatje viaje5Vuelta = new ViatjeFlexible( "Alicante-Alcoy",
                        LocalDateTime.of(2022, 4, 9, 18, 0, 0), 45, 4f, 2, "Pepe");
                viajes.add(viaje5Vuelta);

                Viatje viaje6 = new ViatjeCancelable("Castellon-Gandia",
                        LocalDateTime.of(2022, 5, 10, 12, 0, 0),
                        30, 8f, 3, "Pepe");
                viaje6.reservar("Juan Magoz", 1);
                viaje6.reservar("Selena Lopez", 1);
                viaje6.reservar("Juan", 1);
                viajes.add(viaje6);
                Viatje viajecancelado = new ViatjeCancelable("Castellon-Alfafara",
                        LocalDateTime.of(2022, 8, 10, 12, 0, 0),
                        30, 8f, 3, "Juan");
                viajescancelados.add(viajecancelado);
            }catch (ReservaNoValidaException| ReservasLlenasException e){
                e.getMessage();
            }

        }

}
