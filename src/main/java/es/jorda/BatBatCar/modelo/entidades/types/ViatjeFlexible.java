package es.jorda.BatBatCar.modelo.entidades.types;

import java.time.LocalDateTime;

public class ViatjeFlexible extends ViatjeCancelable{
    public ViatjeFlexible(String ruta, LocalDateTime fechaSalida, long duracion, float precio, int plazasOfertadas, String owner) {
        super(ruta, fechaSalida, duracion, precio, plazasOfertadas, owner);
    }

    @Override
    public String getTipo(){
        return "Viaje Flexible";
    }
}
