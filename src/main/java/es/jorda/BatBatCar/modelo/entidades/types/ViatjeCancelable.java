package es.jorda.BatBatCar.modelo.entidades.types;

import java.time.LocalDateTime;

public class ViatjeCancelable extends Viatje {

    public ViatjeCancelable(String ruta, LocalDateTime fechaSalida, long duracion, float precio, int plazasOfertadas, String owner) {
        super(ruta, fechaSalida, duracion, precio, plazasOfertadas, owner);
    }

    @Override
    public String getTipo(){
        return "Viaje Cancelable";
    }

}
