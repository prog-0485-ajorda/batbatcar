package es.jorda.BatBatCar.modelo.entidades.types;

import java.time.LocalDateTime;

public class ViatjeExclusivo extends Viatje {

    public ViatjeExclusivo(String ruta, LocalDateTime fechaSalida, long duracion, float precio, String owner) {
        super(ruta, fechaSalida, duracion, precio, 1, owner);
    }

    @Override
    public String getTipo(){
        return "Viaje Exclusivo";
    }
}
