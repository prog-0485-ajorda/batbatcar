package es.jorda.BatBatCar.modelo.entidades;

import java.time.LocalDateTime;


public class Reserva {

    private static int accouter = 1;

    private int reserveCode;

    private String user;

    private int requestedPlaces;

    private LocalDateTime data;

    public Reserva(String user, int requestedPlaces){
        this.reserveCode = accouter;
        accouter++;
        this.user = user;
        this.requestedPlaces = requestedPlaces;
        this.data = LocalDateTime.now();
    }

    public int getReservCode(){
        return reserveCode;
    }

    public String getUser(){
        return user;
    }

    public int getRequestedPlaces() {
        return requestedPlaces;
    }

    public LocalDateTime getData(){
        return data;
    }


}
