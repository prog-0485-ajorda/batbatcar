package es.jorda.BatBatCar.modelo.entidades.types;

import es.jorda.BatBatCar.exceptions.ReservaNoValidaException;
import es.jorda.BatBatCar.exceptions.ReservasLlenasException;
import es.jorda.BatBatCar.modelo.entidades.Reserva;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Viatje {

    private int codViaje;

    private String ruta;

    private LocalDateTime fechaSalida;

    private long duracion;

    private float precio;

    protected int plazasOfertadas;

    protected boolean  cerrado;

    private Reserva reserve;

    private String owner;

    private ArrayList<Reserva> reserveMade = new ArrayList<>();

    private static int accouter = 1;

    public Viatje(String ruta, LocalDateTime fechaSalida, long duracion, float precio, int plazasOfertadas, String owner) {
        this.codViaje = accouter;
        accouter++;
        this.ruta = ruta;
        this.fechaSalida = fechaSalida;
        this.duracion = duracion;
        this.precio = precio;
        this.plazasOfertadas = plazasOfertadas;
        this.owner = owner;
        this.cerrado = this.fechaSalida.isBefore(LocalDateTime.now());
    }

    public Reserva reservar(String usuario, int numPlazas) throws ReservaNoValidaException, ReservasLlenasException {
        if (this.plazasOfertadas == reserveMade.size()){
            throw new ReservasLlenasException();
        }
        if (!usuario.equals(this.owner) && numPlazas <= plazasOfertadas && numPlazas > 0){
            Reserva reserva = new Reserva(usuario, numPlazas);
            reserveMade.add(reserva);
            return reserva;
        }
        throw new ReservaNoValidaException();
    }

    public void encontrarReserva(int codigoReseva){
        Reserva reservaAcancelar = null;
        for (Reserva reserva: reserveMade) {
            if (codigoReseva == reserva.getReservCode()){
                reservaAcancelar = reserva;
            }
        }
        if (cancelarReserva(reservaAcancelar)){
            System.out.println("Reserva cancelada");
            return;
        }
        System.out.println("Reserva no encontrada");
    }

    public boolean cancelarReserva(Reserva reserva) {
        if (reserveMade.contains(reserva) || reserva.getData() == fechaSalida){
            reserveMade.remove(reserva);
            return true;
        }

        return false;
    }

    public ArrayList<Reserva> getListaReservas(){
        return reserveMade;
    }

    public boolean isCerrado() {
        return cerrado;
    }

    public int getCodViaje(){
        return codViaje;
    }

    public float getPrecio(){
        return precio;
    }

    public String getRuta(){
        return ruta;
    }

    public LocalDateTime getFechaSalida(){
        return fechaSalida;
    }

    public String getTipo(){
        return "Viaje";
    }

    public int getPlazasDisponibles(){
        return (plazasOfertadas - reserveMade.size());
    }
}

