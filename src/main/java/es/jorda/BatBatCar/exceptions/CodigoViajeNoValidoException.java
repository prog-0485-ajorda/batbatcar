package es.jorda.BatBatCar.exceptions;

public class CodigoViajeNoValidoException extends Exception{
    public CodigoViajeNoValidoException(){super("Error, codigo de viaje no valido");}
}
