package es.jorda.BatBatCar.exceptions;

public class ReservasLlenasException extends Exception{
    public ReservasLlenasException(){super("Error, el cupon de reservas para este viaje ya se ha completado");}
}
