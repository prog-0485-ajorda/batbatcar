package es.jorda.BatBatCar.exceptions;

public class ReservaNoValidaException extends Exception{
    public ReservaNoValidaException(){super("Error, reserva no valida, el conductor no puede reservar en " +
            "su propio viaje y las plazas reservadas" +
            "no pueden ser mas de las ofertadas");}
}
